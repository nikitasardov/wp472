<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mailplugin');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J~&i;F4E[<vK=&qNs4zx7^g,VB4T9izxc~Ai+j7Z@[S>Z/>q;Boz!`1]SESE3#,R');
define('SECURE_AUTH_KEY',  'ivBDK^T]3u365?+X[-%{3YYf<i/%]6?%%t gn!.1Ge/hd;CT:cdnW:;Nd<DfGH82');
define('LOGGED_IN_KEY',    'AYJdh=[j[_G2|Ahq)lc?@w_e*6Ur;i`[$,e(6q`J[xsgQh*Y[=M1WI>%XRkUa{LR');
define('NONCE_KEY',        ';}4.Kx7Rgcj.Z{ZzU73AxH_~y^1 U!QA|jK/|W5Hg[+x;JA}@@hT73sCKsFv4GEV');
define('AUTH_SALT',        'E32VzJaPqVixJV&*+aYLR5dpV0,08sKV%}.W/6E)(LYy/=QgKO8Vmri$=WFsqh|c');
define('SECURE_AUTH_SALT', '* skIpmR]]h?M0JEX?%:qdXo6$G)?2ZY4~2GNe|EjxE!HG!CYlrs%/{vHDUIoBiP');
define('LOGGED_IN_SALT',   '(bzgdy=o}Zs;jI,Ac}jQ! JA@Xh!_-ib[PH#jQ(*.n..>3BYxt`005~Xl=&qR%yg');
define('NONCE_SALT',       '#sS@H@Q=^})he`x{so[<2JzkFeGaAf>GZ+g`;tbO*K0huk2NZj+m}z*!L/}~+OjB');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD','direct');